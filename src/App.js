import './App.css';
import {  BrowserRouter as Router, Switch, Route } from 'react-router-dom'; 
import Home from './pages/Home';
import Currency from './pages/Currency';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          {/* satu route */}
          <Route 
            exact
            path="/"
          >
            <Home />
          </Route>
          {/* route yang lain */}
          <Route 
            exact
            path="/currency"
          >
            <Currency />
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
