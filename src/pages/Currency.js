// handle tampilan di route /currency
import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Container, Row, Col, Button } from 'reactstrap';
import axios from 'axios';

function Currency() {

  // definisi Hooks
  const [ rupiah, setRupiah ] = useState(0);
  const [ target, setTarget ] = useState('USD');
  const [ result, setResult ] = useState(0);
  const [ isDone, setDone ] = useState(false);
  const [ today, setToday ] = useState(null);

  const changeRupiah = (e) => { setRupiah(e.target.value) };
  const changeTarget = (e) => { setTarget(e.target.value) };

  async function convert(rupiah, target) {
    const result = await axios.get(`https://api.ratesapi.io/api/latest?base=IDR&symbols=${target}`);
    const conversionValue = result.data.rates[target];
    setResult(rupiah * conversionValue);
    setToday(result.data.date);
    setDone(true);
  }

  return (
    <Container className="bg-info text-white">
      <h3 className="my-5 pt-3">Konversi Rupiah ke Mata Uang Lain</h3>
      <Row>
        <Col sm="12" md={{ size: 6, offset: 3 }}>
          <Form>
          <FormGroup>
            <Label for="rupiah">Uang dalam rupiah</Label>
            <Input name="rupiah" id="rupiah" placeholder="Rp ..." onChange={changeRupiah}/>
            <Label for="convert">Konversi ke</Label>
            <Input type="select" name="convert" id="convert" onChange={changeTarget}>
              <option value="USD" >USD</option>
              <option value="EUR" >EUR</option>
              <option value="JPY" >JPY</option>
            </Input>
            <Button block className="mt-3" color="success" onClick={() => convert(rupiah, target)}>Konversi</Button>
          </FormGroup>  
          </ Form>
          <h3>{ !isDone ? '' : `Rp${rupiah} setara ${target} ${result} pada ${today}`}</h3>
        </Col>
      </Row>
    </Container>
  );
}

export default Currency;